package SampleTestNGSCripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.Datatable.Xls_Reader;

public class DPfromXL {

	public static Xls_Reader datatable = null;
	public WebDriver driver;

	@BeforeSuite
	public void init() {
		System.out.println("I am in INIT");
		datatable = new Xls_Reader(System.getProperty("user.dir") + "\\src\\com\\Datatable\\Controller.xlsx");
		System.out.println("I have pointed to controller");
		System.setProperty("webdriver.chrome.driver", "Z:\\Selenium\\Jigna_Khara\\TestNG\\Lib\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://10.10.73.117");

	}

	@Test(priority = 1, dataProvider = "getData")
	public void login(String ClusterIP, String PrismIPAddress, String username, String password)
			throws InterruptedException {
		System.out.println("i am starting");
		WebDriverWait wait = new WebDriverWait(driver, 10);
		driver.get(ClusterIP);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("inputPrsimCenterIp")));
		driver.findElement((By.id("inputPrsimCenterIp"))).sendKeys(PrismIPAddress);
		driver.findElement(By.id("inputUsername")).sendKeys(username);
		driver.findElement(By.id("inputPassword")).sendKeys(password);
		driver.findElement(By.xpath("//*[@id=\"loginPassword\"]/label/div")).click();
		System.out.println("i have logged in");
		// System.out.println("i am implicitly waiting 40 secs");
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		// System.out.println("i am done implicitly waiting 40 secs");
	}

	@DataProvider
	public Object[][] getData() {
		System.out.println("I am in getdata");
		String sheetName = "LoginTest";
		System.out.println("My sheetname is: " + sheetName);

		int rows = datatable.getRowCount(sheetName);
		System.out.println("The number of rows is " + rows);
		int cols = datatable.getColumnCount(sheetName);
		System.out.println(cols);
		Object[][] data = new Object[rows - 1][cols];
		for (int rownum = 2; rownum <= rows; rownum++) {
			for (int colnum = 0; colnum < cols; colnum++) {
				data[rownum - 2][colnum] = datatable.getCellData(sheetName, colnum, rownum);
			}

		}
		return data;

	}

}
