package com.Selenium.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.Selenium.Config.BrowserFactory;
import com.Selenium.TestSuite.TestBase;

public class LoginPage extends TestBase {
	public LoginPage(WebDriver driver) throws Throwable {
		//Initialize PageFactory
		 
		
		PageFactory.initElements(BrowserFactory.getBrowser(), this);
		logger.info ("Initialised LoginPage");
	}
		
		/**
		 * Id for Prism Center IP
		 */
		 @FindBy(how = How.ID, using = "inputPrsimCenterIp") 
		 public WebElement PrismCenterIP;
		 
		 /**
		  * ID for Username
		  */
		 
		 @FindBy(how = How.ID, using = "inputUsername") 
		 public WebElement UserName;
		
		 /**
		  * Id for Password
		  */
		 @FindBy(how = How.ID, using = "inputPassword") 
		 public WebElement Password;
		 
		 /**
		  * XPath for SubmitARrow Arrow
		  */
		 @FindBy(how = How.XPATH, using = "//*[@id=\'loginPassword\']/label/div/svg") 
		 public WebElement SubmitArrow;
		 
		 /**
		  * Home Element on home screen
		  */
		 @FindBy(how = How.CLASS_NAME, using = "n-nav-label") 
		 public WebElement HomeElement;

}
