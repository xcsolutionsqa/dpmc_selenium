package com.Selenium.PageObjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.Selenium.Config.BrowserFactory;
import com.Selenium.TestSuite.TestBase;

public class DPMCHomePage extends TestBase {
	public DPMCHomePage(WebDriver driver) throws Throwable {
		//Initialize PageFactory		 
		
		PageFactory.initElements(BrowserFactory.getBrowser(), this);
		logger.info ("Initialised DPMCHomePage");
	
		
	}
		
		/**
		 * Id for Avamar Server Summary
		 */
		 @FindBy(how = How.CSS, using = "span.lblTitle.title[title='Avamar Server Summary']") 
		 public WebElement AvamarServerSum;
		 
		 /**
		  * Home Element on home screen
		  */
		 @FindBy(how = How.CLASS_NAME, using = "n-nav-label") 
		 public WebElement HomeElement;
		 
		 /**
		  * User Icon on Home screen which is in the top right corner
		  */
		 
		 @FindBy(how = How.CLASS_NAME, using = "n-username") 
		 public WebElement UserIcon;

		 /**
		  * About Dell EMC Option Under Admin menu
		  */
		 @FindBy(how = How.LINK_TEXT, using="About Dell EMC")
		 public WebElement AboutDellEMC;
		 
		 /**
		  * Sign Out Option under Admin menu
		  */
		 public WebElement SignOut;
		 
		 /**
		  * OK Button on pop up for About Dell EMC
		  */
		 @FindBy(how = How.CSS, using="#popupVersion > div.n-modal-wrapper > div.modal-footer > button")
		 public WebElement btnOK;
		 
		 /**
		  * About DEll EMC pop up
		  */
		 @FindBy(how = How.CLASS_NAME, using="n-title")
		 public WebElement AboutDellEMCTitle;
		 
		 /**
		  * About Dell EMC Product logo
		  */
		 @FindBy(how = How.CLASS_NAME, using="n-product-logo")
		 public WebElement AboutDellEMCProductLogo;
		 
		 /**
		  * About Dell EMC product version name
		  */
		 
		 @FindBy(how = How.CSS, using="#popupVersion > div.n-modal-wrapper > div.n-modal-body > div > div > div.n-product-version > div:nth-child(1)")
		 public WebElement AboutDellEMCProductVersionName;
		 
		 /**
		  * About Dell EMC product version number
		  */
		 @FindBy(how = How.CLASS_NAME, using="lblVersion")
		 public WebElement AboutDellEMCProductVersionNumber;
		 
		 /**
		  * Setting Menu Gear Button: 
		  */
		 @FindBy(how = How.XPATH, using="//*[@id=\"n-header\"]/div/div/div/div/div[8]/ul/li[3]/a")
		 public WebElement SettingsGear;
		 
		 /**
		  * AAvamar REgistration option under Gear settings menu
		  */
		 @FindBy(how = How.LINK_TEXT, using="Avamar Registration")
		 public WebElement AvamarRegistration;
		 
		 /**
		  * Pop up for Avamar Registration
		  */
		 
		 @FindBy(how=How.ID, using="popupAvamarRegistration")
		 public WebElement popUpAvamarRegistration;
		 
		 /**
		  * Avamar Registration table
		  */
		 @FindBy(how=How.ID, using="avamarList")
		 public WebElement AvamarRegistrationTable;
		 
		 /**
		  * Avamar registration settings Edit Button
		  */
		 @FindBy(how=How.CLASS_NAME, using="btnEdit")
		 public WebElement btnEdit;
		 
		 /**
		  * Update Avamar title pop up
		  */
		 @FindBy(how=How.CSS, using="#popupAvamarRegistration > div.modal-header > h4")
		 public WebElement UpdateAvamarTitle;
		 
		 /**
		  * Update Avamar Hostname input box
		  */
		 @FindBy(how=How.CLASS_NAME, using="inputHostName")
		 public WebElement UpdateAvamarHostname;
		 
		 /**
		  * Update Avamar IP input box
		  */
		 @FindBy(how=How.CLASS_NAME, using="inputIPAddress")
		 public WebElement UpdateAvamarIP;
		 
		 /**
		  * Update Avamar User input box
		  */
		 @FindBy(how=How.CLASS_NAME, using="inputUserName")
		 public WebElement UpdateAvamarUsername;
		 
		 /**
		  * Update Avamar Password input box
		  */
		 @FindBy(how=How.CLASS_NAME, using="inputUserName")
		 public WebElement UpdateAvamarPass;
		 
		 
		 /**
		  * Update Avamar Back Button
		  */
		 @FindBy(how=How.CSS, using="#popupAvamarRegistration > div.n-modal-wrapper > div.n-modal-footer > button.btnBack.btn.n-btn-form.secondary-btn.pull-left")
		 public WebElement btnUpdateAvamarBack;
		 /**
		  * Update Avamar Cancel Button
		  */
		 @FindBy(how=How.CSS, using="#popupAvamarRegistration > div.n-modal-wrapper > div.n-modal-footer > button.btnCancel.btn.n-btn-form.secondary-btn")
		 public WebElement btnUpdateAvamarCancel;
		 
}
