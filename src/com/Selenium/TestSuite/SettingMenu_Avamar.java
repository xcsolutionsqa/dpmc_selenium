package com.Selenium.TestSuite;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.Selenium.Utils.Keywords;
import com.Selenium.Config.BrowserFactory;
import com.Selenium.PageObjects.DPMCHomePage;


public class SettingMenu_Avamar extends TestBase {
	
	private DPMCHomePage HomePage;
	
	
	@BeforeTest
	  public void Initialise_Setting_Menu() throws Throwable {
		HomePage=PageFactory.initElements(BrowserFactory.getBrowser(), DPMCHomePage.class);
		
	  }
  @Test(dependsOnMethods={"com.Selenium.TestSuite.LoginToDPMC.LoginTest"})
  public void XCS_TC_693_AvamarREgistrationValidation() throws InterruptedException {
	  logger.info("In XCS_TC_693_AvamarREgistration");
	
	  wait.until(ExpectedConditions.elementToBeClickable(HomePage.SettingsGear));	  
	  HomePage.SettingsGear.click();
	  wait.until(ExpectedConditions.elementToBeClickable( HomePage.AvamarRegistration));
	  HomePage.AvamarRegistration.click();
	  wait.until(ExpectedConditions.visibilityOf(HomePage.popUpAvamarRegistration));
	  Assert.assertEquals(true,HomePage.popUpAvamarRegistration.isDisplayed());  
	  logger.info("Opened Avamar Registration Popup");
	  //Validate table data is not null
	  wait.until(ExpectedConditions.visibilityOf(HomePage.AvamarRegistrationTable));
	  String AvamarName=Keywords.GetTableData(1,0, HomePage.AvamarRegistrationTable);
	  logger.info("Data in the table is:AvamarName: "+AvamarName);	  
	  Assert.assertEquals(false, Keywords.GetTableData(1,0, HomePage.AvamarRegistrationTable).isEmpty());
	  String AvamarIP=Keywords.GetTableData(1,1, HomePage.AvamarRegistrationTable);
	  logger.info("Data in the table is:AvamarIP: "+AvamarIP);
	  Assert.assertEquals(false, Keywords.GetTableData(1,1, HomePage.AvamarRegistrationTable).isEmpty());
	  logger.info("Completed validation of  Avamar Registration Popup");
	  //Validate Edit Avamar settings
	  HomePage.btnEdit.click();
	  wait.until(ExpectedConditions.visibilityOf(HomePage.UpdateAvamarTitle));
	  Assert.assertEquals(true, HomePage.UpdateAvamarTitle.isDisplayed());
	  Assert.assertEquals(false, HomePage.UpdateAvamarHostname.isEnabled());
	  Assert.assertEquals(HomePage.UpdateAvamarHostname.getAttribute("Value"),AvamarName);
	  Assert.assertEquals(true, HomePage.UpdateAvamarIP.isEnabled());
	  Assert.assertEquals(HomePage.UpdateAvamarIP.getAttribute("Value"),AvamarIP);
	  Assert.assertEquals(false, HomePage.UpdateAvamarUsername.isEnabled());
	  Assert.assertEquals(false, HomePage.UpdateAvamarPass.isEnabled());
	  logger.info("Completed validation of  Avamar Registration Edit settings");
	  //Verify action of Back button
	  HomePage.btnUpdateAvamarBack.click();
	  wait.until(ExpectedConditions.visibilityOf(HomePage.popUpAvamarRegistration));
	  Assert.assertEquals(true,HomePage.popUpAvamarRegistration.isDisplayed());  
	  logger.info("Completed validation of  Avamar Registration Edit settings Back Button. Page navigated to Avamar registration popup");
	  wait.until(ExpectedConditions.elementToBeClickable(HomePage.btnEdit));
	  //Verify action of cancel Button
	  HomePage.btnEdit.click();
	  wait.until(ExpectedConditions.elementToBeClickable(HomePage.btnUpdateAvamarCancel));
	  HomePage.btnUpdateAvamarCancel.click();
	  wait.until(ExpectedConditions.visibilityOf(HomePage.SettingsGear));	
	  Assert.assertEquals(true,HomePage.SettingsGear.isDisplayed());
	  logger.info("Completed validation of  Avamar Registration edit settings Cancel Button. Page navigated to DPMC Home Page");
	  
  }
}
