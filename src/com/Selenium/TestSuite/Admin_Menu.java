package com.Selenium.TestSuite;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.Selenium.Config.BrowserFactory;
import com.Selenium.PageObjects.DPMCHomePage;
import com.Selenium.PageObjects.LoginPage;
import com.Selenium.Utils.TestUtils;

public class Admin_Menu extends TestBase{
	private DPMCHomePage HomePage;
	private LoginPage LogPage;
	
	
  @BeforeTest
  public void Initialise_Admin_Menu() throws Throwable {
	HomePage=PageFactory.initElements(BrowserFactory.getBrowser(), DPMCHomePage.class);
	LogPage=PageFactory.initElements(BrowserFactory.getBrowser(), LoginPage.class);
  }
  @Test(dependsOnMethods={"com.Selenium.TestSuite.LoginToDPMC.LoginTest"})
  public void XCS_TC_704_AboutDellEMCValidation() throws Throwable {
	  try {
		  logger.info("In XCS_TC_704_AboutDellEMCValidation ");		  
		  HomePage.UserIcon.click();
		  wait.until(ExpectedConditions.presenceOfElementLocated(By.className(HomePage.AboutDellEMCTitle.getAttribute("class"))));
		  HomePage.AboutDellEMC.click();
		  logger.info("Clicked on About Dell EMC");	  
		  Thread.sleep(2000);
		  Assert.assertEquals(true,HomePage.AboutDellEMCTitle.isDisplayed());
		  Assert.assertEquals(true,HomePage.AboutDellEMCProductLogo.isDisplayed());
		  Assert.assertEquals(true,HomePage.AboutDellEMCProductVersionName.isDisplayed());
		  Assert.assertEquals(true,HomePage.AboutDellEMCProductVersionNumber.isDisplayed());
		  logger.info("Verified all components present");
		  HomePage.btnOK.click();
		  Thread.sleep(3000);
	  }
	  catch(Exception e) {
		  logger.debug(e.getMessage());
		  TestUtils.TakeScreenShot("XCS_TC_704_AboutDellEMCValidation");
	  }
	  
  }
  
  @Test(dependsOnMethods={"com.Selenium.TestSuite.LoginToDPMC.LoginTest"})
  public void XCS_TC_714_SignOutValidation() throws Throwable {
	  try 
	  {
		  logger.info("In XCS_TC_714_SignOutValidation");
		  HomePage.UserIcon.click();
		  HomePage.SignOut.click();
		  Assert.assertEquals(true,LogPage.PrismCenterIP.isDisplayed());		  
	  }
	  catch(Exception e) {
		  logger.debug(e.getMessage());
		  TestUtils.TakeScreenShot("XCS_TC_714_SignOutValidation");
	  }
	  
  }
	  
  
  
 
}
