package com.Selenium.TestSuite;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.Selenium.Config.BrowserFactory;
import com.Selenium.PageObjects.DPMCHomePage;
import com.Selenium.PageObjects.LoginPage;
import com.Selenium.Utils.TestUtils;


public class LoginToDPMC extends TestBase  {
	
	private LoginPage LogPage;	
	private DPMCHomePage HomePage;
	
	
	@BeforeTest
	public void InitialiseLogin() throws Throwable {
		LogPage=PageFactory.initElements(BrowserFactory.getBrowser(), LoginPage.class);
		HomePage=PageFactory.initElements(BrowserFactory.getBrowser(), DPMCHomePage.class);
	}
	
  @Test(priority = 1, dataProvider = "getData" )
  public void LoginTest(String PrismIPAddress, String username, String password) throws Throwable {
	  try {
		  logger.debug("In LoginTest");
		  logger.info("The Data coming is: "+PrismIPAddress+" "+username+" "+password);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LogPage.PrismCenterIP.getAttribute("id"))));
		  LogPage.PrismCenterIP.sendKeys(PrismIPAddress);
		  LogPage.UserName.sendKeys(username);
		  LogPage.Password.sendKeys(password);	  
		  LogPage.Password.sendKeys(Keys.ENTER);
		  wait.until(ExpectedConditions.visibilityOf(HomePage.HomeElement));		  
		  Assert.assertEquals(true,HomePage.HomeElement.isDisplayed());  
		  
	  }
	  catch(Exception e) {
		 logger.debug(e.getMessage());
		 TestUtils.TakeScreenShot("LoginTest");
	  }
	  
	  
  }
  
  @AfterSuite
  public void CloseBrowser() {
	  driver.close();
  }
  
  
  
  @DataProvider
	public Object[][] getData() {		
	  logger.info("In getdata dataprovider method");
		String sheetName = "LoginTest";
		logger.info("sheetname is: " + sheetName);
		int rows = datatable.getRowCount(sheetName);
		logger.info("Number of rows in xcel sheet is " + rows);
		int cols = datatable.getColumnCount(sheetName);
		logger.info("Number of rows in xcel sheet is "+cols);
		Object[][] data = new Object[rows - 1][cols];
		for (int rownum = 2; rownum <= rows; rownum++) {
			for (int colnum = 0; colnum < cols; colnum++) {
				data[rownum - 2][colnum] = datatable.getCellData(sheetName, colnum, rownum);
			}
		}
		logger.info("Returning data from xcel sheet");
		return data;
	}
	
	
  
}
