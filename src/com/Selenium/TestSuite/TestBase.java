package com.Selenium.TestSuite;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.Datatable.Xls_Reader;
import com.Selenium.Config.BrowserFactory;
import com.Selenium.Utils.Keywords;

//Initialize Config, xl,or properties

public class TestBase {
	public static Properties config=null;
	public static WebDriver driver=null;
	public static WebDriverWait wait=null;
	public static Logger logger;
	public static Xls_Reader datatable = null;//to read data from XLS
	
	@BeforeSuite
	

	public void initialize() throws Throwable   {
		{
			//Log file
			logger=Logger.getLogger("DPMCLogger");
			PropertyConfigurator.configure("log4j.properties");
			//Read config.properties
			config=new Properties();
			FileInputStream fp= new FileInputStream(System.getProperty("user.dir")+"\\src\\com\\Selenium\\Config\\config.Properties");
			config.load(fp);
			//Open Browser		
			driver=BrowserFactory.getBrowser();
			driver.get(config.getProperty("URL"));
			Keywords.MaximiseWindow();
			//Xcel sheet initialize	
			datatable = new Xls_Reader(System.getProperty("user.dir") + "\\src\\com\\Datatable\\Controller.xlsx");
			logger.debug("Controller.xls is at:  "+datatable.path);
			//Initialize wait time
			wait = new WebDriverWait(driver, 10);
		}
		
		
	}
	
}
