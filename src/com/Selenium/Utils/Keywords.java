package com.Selenium.Utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.Selenium.TestSuite.TestBase;

public class Keywords extends TestBase {
 
  public static void MaximiseWindow() {
	  driver.manage().window().maximize();
  }
  public static String GetTableData(int row,int column,WebElement Table) {
	  
	   
	   List<WebElement> NoOfRows=Table.findElements(By.tagName("tr"));
	  List<WebElement> NoOfCols=NoOfRows.get(row).findElements(By.tagName("td"));
	  
	  return NoOfCols.get(column).getText();
	  
 }
  
}
