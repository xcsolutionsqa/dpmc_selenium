package com.Selenium.Utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

import com.Selenium.TestSuite.TestBase;

public class TestUtils extends TestBase {
 
  public static void TakeScreenShot(String TestName) throws  Throwable {
	  File Srcfile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	  Date mydate=new Date();	  
	  System.out.println("Today is: "+mydate.toString());
	  FileUtils.copyFile(Srcfile, new File("C:\\Users\\Jigna_Khara\\eclipse-workspace\\TestNG\\src\\ScreenShots"+"\\"+TestName+new SimpleDateFormat("MM-dd-yyyy-hh-mm-ss").format(mydate)+".png"));
	  
  }
  
  
}
