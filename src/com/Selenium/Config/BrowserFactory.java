package com.Selenium.Config;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.Selenium.TestSuite.TestBase;

public class BrowserFactory extends TestBase {

	private static Map<String, WebDriver> drivers = new HashMap<String, WebDriver>();
	/*
	 * Factory method for getting browsers
	 */
	public static WebDriver getBrowser() throws Throwable {
		String browserName=config.getProperty("BrowserType");
		
		 WebDriver driver = null;
		switch (browserName) {
		/*case "Firefox":
			driver = drivers.get("Firefox");
			if (driver == null) {
				driver = new FirefoxDriver();
				drivers.put("Firefox", driver);
			}
			break;
		case "IE":
			driver = drivers.get("IE");
			if (driver == null) {
				System.setProperty("webdriver.ie.driver",
						"C:\\Users\\abc\\Desktop\\Server\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				drivers.put("IE", driver);
			}
			break;*/
		case "CHROME":
			driver = drivers.get("CHROME");
			if (driver == null) {
				System.setProperty("webdriver.chrome.driver","Z:\\Selenium\\Jigna_Khara\\TestNG\\Lib\\chromedriver.exe");
				driver = new ChromeDriver();
				drivers.put("CHROME", driver);
			}
			break;
		}
		return driver;
	}
	
	public static void closeAllDriver() {
		for (String key : drivers.keySet()) {
			drivers.get(key).close();
			drivers.get(key).quit();
		}
	}
}
